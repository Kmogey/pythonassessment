import re

ptn2= re.compile("#.*")
HostRealptn=re.compile("([a-fA-F0-9]{2}[:|\-]?){6}")
ptn= re.compile("\s+")
patern =re.compile("(([2][5][0-5]\.)|([2][0-4][0-9]\.)|([0-1]?[0-9]?[0-9]\.)){3}(([2][5][0-5])|([2][0-4][0-9])|([0-1]?[0-9]?[0-9]))")

HostReal=open("host.real", "r")
outhr=open("tmp.txt", "w")
IPWrite=open("ipwriet.txt" ,"w")

for line in HostReal:
    if re.search("^#",line):
        continue

    if re.search('#',line):
        line=ptn2.sub("",line)
    data=ptn.split(line)
    outhr.write(" ".join(data)+"\n")

    realhost=HostRealptn.search(line)
    if realhost:
            print(realhost.group())

    adress=patern.search(line)
    if adress:
        print(adress.group())
    IPWrite.write(data[0]+"\n")
HostReal.close()
outhr.close()
IPWrite.close()
